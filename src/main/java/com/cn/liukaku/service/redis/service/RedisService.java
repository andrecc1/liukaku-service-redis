package com.cn.liukaku.service.redis.service;

public interface RedisService {

    /**
     *
     * @param key
     * @param value
     * @param seconds
     */
    public void put(String key,Object value,long seconds);

    /**
     *
     * @param key
     */
    public Object get(String key);

}
