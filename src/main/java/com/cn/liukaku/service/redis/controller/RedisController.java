package com.cn.liukaku.service.redis.controller;

import com.cn.liukaku.service.redis.service.RedisService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class RedisController {

    @Resource
    private RedisService redisService;

    @RequestMapping(value = "put", method = RequestMethod.POST)
    public String put(String key, String value, long seconds) {
        redisService.put(key, value, seconds);
        return "ok";
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public Object get(String key) {

        Object obj = redisService.get(key);

        if (obj != null) {
            String json = String.valueOf(obj);
            return json;
        }
        return null;
    }

}
